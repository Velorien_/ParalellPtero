﻿using System;
using System.Diagnostics;
using System.Threading;

namespace SimplyThreads
{
    public class ThreadSample
    {
        private static readonly Random Random = new Random();

        private static AutoResetEvent autoResetEvent = new AutoResetEvent(true);

        private object threadLock = new object();

        public void SomeAction()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            string threadName = Thread.CurrentThread.Name;
            Console.WriteLine($"Thread {threadName} has started...");

            int sleepTime = Random.Next(500, 5000);
            Console.WriteLine($"Thread {threadName} will now sleep for {sleepTime} ms");
            Thread.Sleep(sleepTime);
            stopwatch.Stop();
            Console.WriteLine($"Thread {threadName} has finished in {stopwatch.ElapsedTicks} ticks");
        }

        public void SomeActionAutoReset()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            string threadName = Thread.CurrentThread.Name;
            Console.WriteLine($"Thread {threadName} has started...");

            for(int i = 0; i < 10; i++)
            {
                autoResetEvent.WaitOne();
                int sleepTime = Random.Next(500, 2000);
                Console.WriteLine($"Thread {threadName} will now sleep for {sleepTime} ms <{i}>");
                Thread.Sleep(sleepTime);
                autoResetEvent.Set();
            }

            Console.WriteLine($"Thread {threadName} has finished in {stopwatch.ElapsedTicks} ticks");
        }

        public void SomeActionWithLock()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            string threadName = Thread.CurrentThread.Name;
            Console.WriteLine($"Thread {threadName} has started...");

            for (int i = 0; i < 10; i++)
            {
                lock (threadLock)
                {
                    int sleepTime = Random.Next(500, 2000);
                    Console.WriteLine($"Thread {threadName} will now sleep for {sleepTime} ms <{i}>");
                    Thread.Sleep(sleepTime);
                }
            }

            Console.WriteLine($"Thread {threadName} has finished in {stopwatch.ElapsedTicks} ticks");
        }
    }
}
