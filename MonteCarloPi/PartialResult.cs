﻿using System;

namespace MonteCarloPi
{
    public class PartialResult
    {
        public int PointsInsideCount { get; set; }

        public TimeSpan TimeElapsed { get; set; }
    }
}
