﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace MonteCarloPi
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<PartialResult> results = new ObservableCollection<PartialResult>();
        private int threadCount = 1;
        private int pointsPerThread = 10000;
        private bool isRunning = false;

        public MainViewModel()
        {
            RunAsync = new RelayCommand(() => CalculatePiAsync(ThreadCount, PointsPerThread));
        }

        public int ProcessorCount
        {
            get { return Environment.ProcessorCount; }
        }

        public RelayCommand RunAsync { get; private set; }

        public ObservableCollection<PartialResult> Results
        {
            get { return results; }
            set
            {
                Set(() => Results, ref results, value);
                RaisePropertyChanged(() => CalculatedPi);
            }
        }

        public int ThreadCount
        {
            get { return threadCount; }
            set { Set(() => ThreadCount, ref threadCount, value); }
        }

        public int PointsPerThread
        {
            get { return pointsPerThread; }
            set { Set(() => PointsPerThread, ref pointsPerThread, value); }
        }

        public bool IsRunning
        {
            get { return isRunning; }
            set { Set(() => IsRunning, ref isRunning, value); }
        }

        public decimal CalculatedPi
        {
            get
            {
                return Results.Sum(x => x.PointsInsideCount) / (decimal)(PointsPerThread * ThreadCount) * 4;
            }
        }

        private async void CalculatePiAsync(int threadCount, int pointsPerThread)
        {
            IsRunning = true;
            Results.Clear();
            var tasks = new List<Task<PartialResult>>();
            for (int i = 0; i < threadCount; i++)
            {
                tasks.Add(PiCalculator.ExecuteAsync(pointsPerThread));
                await Task.Delay(10);
            }

            var taskResults = await Task.WhenAll(tasks);

            IsRunning = false;
            Results = new ObservableCollection<PartialResult>(taskResults);
        }
    }
}
